# README #

Version 0.0.1

Welcome to Zoomer. An XSplit Extension with the power to zoom in on camera sources.

### How do I get set up? ###

* Install Yarn using npm install yarn --g
* Run `yarn` from the root directory
* Use `gulp` from the root directory to start a dev server with hot reloading
* In XSplit, add Extension and then enter the local IP - http://localhost:9001/zoomer.html