const xjs = require('xjs');
const Scene = xjs.Scene;

const TYPE_GAMESOURCE = 7;
const TYPE_LIVE = 2;
const TYPE_IMAGE = 4;

let CurrentScene;

Scene.getActiveScene().then((scene) => {
    CurrentScene = scene;
    CurrentScene.getItems().then((items) => { 
        for(let i = 0; i < items.length; i++) {
            items[i].getType().then((type) => { 
                if(type == TYPE_LIVE) {                    
                    const newElement = document.createElement('BUTTON');
                    const text = document.createTextNode('Source: ' + items[i]._name);
                    newElement.setAttribute('data-id', items[i]._id);
                    newElement.appendChild(text);
                    newElement.addEventListener('click', setObjectZoom);
                    document.getElementById('item-list').appendChild(newElement);
                }
            });
        }
    });

});

function setObjectZoom(e) {
    Scene.searchItemsById(e.target.getAttribute('data-id')).then((item) => {
        animatedZoom(0.8, item);
    });
}

function animatedZoom(zoomAmount, item) {
    const duration = 5000;
    const fps = 60;
    const interval = 1000 / fps;
    const zoomFactor = zoomAmount / 2;

    let elapsedTime = 0;

    const timer = setInterval(() => {
        if(Math.round(elapsedTime) >= duration) {
            return clearInterval(timer);
        }
        let tmpValue = zoomFactor * (elapsedTime / duration);
        item.setCropping({left: tmpValue, top: tmpValue, right: tmpValue, bottom: tmpValue});
        elapsedTime += interval;
    }, interval);
}

function calculateZoom(zoom, crop) { // We'd probably put the canvas/mixer size as a param here or inside of the actual function.
    let zoomedCrop = {};
    let zoomFactor = zoom / 2;
    zoomedCrop.left = zoomFactor;
    zoomedCrop.top =  zoomFactor;
    zoomedCrop.right = zoomFactor;
    zoomedCrop.bottom = zoomFactor;
    return zoomedCrop;
}

//TODO: Create a function to animate the cropping, maybe set a global frame rate value which will update the values at intervals
//TODO: Add an origin/anchor point option to choose which part you want it to zoom on.
//TODO: Make it more interactive so you can search where to zoom, maybe use scroll wheel and position of mouse.